/**
 * @file    logger.h
 * @brief   cppSimpleLogger Header
 * @author  hendrik schutter
 * @date    04.09.2020
 */

#ifndef LOGGER_H_
#define LOGGER_H_

#include <time.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <net/if.h>
#include <unistd.h>
#include <sys/time.h>
#include <math.h>
#include <mutex>

using namespace std;

#define MENU_LINE_SIZE 110 //Size of menu lines

#ifndef LOG_PATH
#define LOG_PATH "./test.txt"
#endif

#ifndef DESCRIPTION
#define DESCRIPTION "Software-Name - Copyright Company 2020"        //use your values here
#endif

#ifndef DEVICE_ID
#define DEVICE_ID  "Device-Name"                                    //use your values here
#endif

#ifndef SOFTWARE_VERSION
#define SOFTWARE_VERSION  "0.1.1.8"                                 //use your values here
#endif

#ifndef HARDWARE_VERSION
#define HARDWARE_VERSION  "7.77.9"                                  //use your values here
#endif

class Logger
{
private:
    string logPath;
    mutex mtxLog;

    static bool instanceFlag;
    static Logger *single;

    string getTimestamp();
    void writeLog(string s);
    string getMacAddress();
    string padStringMenu(char cBorder, string text, uint8_t u8LineLenght);
    string menuLine(char cBorder, uint8_t u8LineLenght);
    Logger();
    ~Logger();

public:

    void info(string s);
    void warning(string s);
    void error(string s);
    void newLine();

    static Logger* logThis();
};

#endif  // LOGGER_H_