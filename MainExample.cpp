/**
 * @file    MainExample.cpp
 * @brief   example using logger
 * @author  hendrik schutter
 * @date    04.09.2020
 */

#include <stdio.h>
#include "logger.h"

using namespace std;

/**
 * \brief   application entry point
 * \param	void
 * \return  int
 */
int main(void)
{
    cout << "Hello World!\n";

    Logger::logThis()->info("Alle Systeme laufen!");                      //demo
    usleep(1465);
    Logger::logThis()->warning("Alle Systeme laufen!");                   //demo
    usleep(51654);
    Logger::logThis()->error("Alle Systeme laufen!");                     //demo

    cout << "bye!\n";

    return 0;
}

